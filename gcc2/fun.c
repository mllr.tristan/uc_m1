

void my_printS(short s) //HEX
{
  int k; char c;
  for(k=3;k>=0;k--)
  {
	  c=s>>(4*k)&0x0f;
	  if(c<10) mon_putchar(c+'0');
		else mon_putchar(c+'A'-10);
  }
   mon_putchar('\r');
   mon_putchar('\n');
 
 }
 
void my_printD(short s) //DEC
{
	int k=10000; char c;
	while(k>0)
	{
		c=s/k; mon_putchar(c+'0');
		s=s-c*k;
		k/=10;
	}
   mon_putchar('\r');
   mon_putchar('\n');
}



