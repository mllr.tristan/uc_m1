#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "common.h"

#define NB_PHILO 5

xSemaphoreHandle xMutex[NB_PHILO];
int assis[NB_PHILO];
int drop[NB_PHILO];
int one[NB_PHILO];
int eaten[NB_PHILO];
int mange[NB_PHILO];

void func(void* p)
{ int numero= *(int*) p;
  while (mange[numero]!=1)
    {xSemaphoreTake( xMutex[NB_PHILO+1], portMAX_DELAY );
    uart_putc('0'+numero);uart_puts(" wants to eat\r\n\0");//vTaskDelay(500 / portTICK_RATE_MS); //Assis
    xSemaphoreGive( xMutex[NB_PHILO+1] );
    assis[numero]=1;
     if (xSemaphoreTake(xMutex[numero],500/portTICK_RATE_MS)==pdFALSE)
        {
         xSemaphoreTake( xMutex[NB_PHILO+1], portMAX_DELAY );
         uart_putc('0'+numero);
         uart_puts("has dropped the chopsticks\r\n\0"); //Drop
         xSemaphoreGive( xMutex[NB_PHILO+1] );
         drop[numero]=1;
         xSemaphoreGive( xMutex[numero] );
        }
     xSemaphoreTake( xMutex[(numero+1)%NB_PHILO], portMAX_DELAY );
     xSemaphoreTake( xMutex[NB_PHILO+1], portMAX_DELAY );
     uart_putc('0'+numero);uart_puts(" take a chopstick\r\n\0");//vTaskDelay(500 / portTICK_RATE_MS); //Prend chop
     xSemaphoreGive( xMutex[NB_PHILO+1] );
     one[numero]=1;
     xSemaphoreGive( xMutex[numero] );
     xSemaphoreGive( xMutex[(numero+1)%NB_PHILO] );
     xSemaphoreTake( xMutex[NB_PHILO+1], portMAX_DELAY );
     uart_putc('0'+numero);uart_puts(" has eaten\r\n\0"); //Has eaten
     xSemaphoreGive( xMutex[NB_PHILO+1] );
     mange[numero]=1;
     eaten[numero]=1;
    }
  while (1) { vTaskDelay(100 / portTICK_RATE_MS); }; // on n'a jamais le droit de finir toutes les taches
}

/*void prompt(void* p)
{
  int i= *(int*) p;
  //for (int i = 0; i < NB_PHILO; i++)
  //{
    if (assis[i] == 1)
    {
      uart_putc('a'+i);vTaskDelay(500 / portTICK_RATE_MS); //Assis
      assis[i]=0;
    }
    
    if (drop[i] == 1)
    {
      uart_putc('u'+i);
      uart_puts("ng\r\n\0"); //Drop
    }

    if (one[i] == 1)  
    {
      uart_putc('A'+i);vTaskDelay(500 / portTICK_RATE_MS); //Prend chop
    }

    if (eaten[i] == 1)
    {
      uart_putc('0'+i); //Has eaten
    }
    
    
    
  //}
  
}*/

int main()
{ 
//www.freertos.org/FreeRTOS_Support_Forum_Archive/February_2007/freertos_Problems_with_passing_parameters_to_task_1666309.html
  static int p[NB_PHILO];
  static char * taskNames[5] = {"P0","P1","P2","P3","P4"};

  int i;
  Usart1_Init();
  Led_Init();
  xMutex[NB_PHILO+1] = xSemaphoreCreateMutex();
  for (i=0;i<NB_PHILO;i++) {xMutex[i] = xSemaphoreCreateMutex();p[i]=i;}
  for (i=0;i<NB_PHILO;i++)
      {xTaskCreate(func, (const signed char const*)taskNames[i], STACK_BYTES(256), (void*)&p[i],1,0);}
  //xTaskCreate(prompt,"Affichage", STACK_BYTES(256), (void*)&p[i],1,0);
  vTaskStartScheduler();
  while(1);
  return 0;
}
