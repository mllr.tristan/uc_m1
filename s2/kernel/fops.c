#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <asm/uaccess.h>

#define MOD_MAJOR 90 //Find Free number with cat /proc/devices

static int mod_open(struct inode *inod, struct file *fil)
{
    printk(KERN_INFO "OPEN\n");
    return 0;
}

static int mod_release(struct inode *inod, struct file *fil)
{
    printk(KERN_INFO "CLOSE\n");
    return 0;
}

static ssize_t mod_read(struct file *fil, char *buff, size_t len, loff_t *off)
{
    printk(KERN_INFO "SOMEONE READ\n");
    return 0;
}

static ssize_t mod_write(struct file *fil, const char *buff, size_t len,loff_t *off)
{
    printk(KERN_INFO "SOMEONE WROTE %s \n",buff);
    return len;
}


static struct file_operations fops =
{
    read:           mod_read,
    write:          mod_write,
    open:           mod_open,
    release:        mod_release,
};

static int __init mod_start(void)
{
    printk(KERN_INFO "Hello\n"); 
    register_chrdev (MOD_MAJOR, "TM", &fops);
    return 0;
}

static void __exit mod_end(void)
{
    printk(KERN_INFO "Goodbye\n");
}



module_init(mod_start);
module_exit(mod_end);

MODULE_LICENSE("GPL");

