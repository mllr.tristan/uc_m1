#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <asm/uaccess.h>

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

#define BCM2711_PERI_BASE  0xFE000000
#define GPIO_BASE         (BCM2711_PERI_BASE+0x200000) // GPIO controller 
#define PAGE_SIZE  (4*1024)
#define BLOCK_SIZE (4*1024)
#define INP_GPIO(g) *(gpio+((g)/10))&= ~(7<<(((g)%10)*3))
#define OUT_GPIO(g) *(gpio+((g)/10))|=(1<<(((g)%10)*3))

#define GPIO_SET *(gpio+7+1)   //sets bits which are 1 ignores bits which are 0
#define GPIO_CLR *(gpio+10+1)  //clears bits which are 1 ignores bits which are 0

#define MOD_MAJOR 90 //Find Free number with cat /proc/devices

static int mod_open(struct inode *inod, struct file *fil)
{
    printk(KERN_INFO "OPEN\n");
    return 0;
}

static int mod_release(struct inode *inod, struct file *fil)
{
    printk(KERN_INFO "CLOSE\n");
    return 0;
}

static ssize_t mod_read(struct file *fil, char *buff, size_t len, loff_t *off)
{
    printk(KERN_INFO "SOMEONE READ\n");
    return 0;
}

static ssize_t mod_write(struct file *fil, const char *buff, size_t len,loff_t *off)
{
    printk(KERN_INFO "SOMEONE WROTE %s \n",buff);
    
    if (request_mem_region(GPIO_BASE,PAGE_SIZE,"GPIO")==NULL)
    {
        unsigned int *gpio;
        int g,rep;
        int mem_fd=open("/dev/mem",O_RDWR|O_SYNC);
        /*gpio=mmap(
                    NULL,                 //Any adddress in our space will do
                    PAGE_SIZE,            //Map length
                    PROT_READ|PROT_WRITE, //Enable reading & writing to mapped memory
                    MAP_SHARED,           //Shared with other processes
                    mem_fd,               //File to map
                    GPIO_BASE             //Offset to GPIO peripheral
                );*/
        
        gpio = ioremap(GPIO_BASE);
        
        INP_GPIO(42);//must use INP_GPIO before we can use OUT_GPIO
        OUT_GPIO(42);
        if (buff > "0\n")
        {    
            GPIO_SET = 1<<10;       
        }
        else 
        {
            GPIO_CLR = 1<<10;
        }
    }
    return len;

}


static struct file_operations fops =
{
    read:           mod_read,
    write:          mod_write,
    open:           mod_open,
    release:        mod_release,
};

static int __init mod_start(void)
{
    printk(KERN_INFO "Hello\n"); 
    register_chrdev (MOD_MAJOR, "TM", &fops);
    return 0;
}

static void __exit mod_end(void)
{
    printk(KERN_INFO "Goodbye\n");
}



module_init(mod_start);
module_exit(mod_end);

MODULE_LICENSE("GPL");

