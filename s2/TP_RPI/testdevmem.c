#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

#define BCM2711_PERI_BASE  0xFE000000
#define GPIO_BASE         (BCM2711_PERI_BASE+0x200000) // GPIO controller 
#define PAGE_SIZE  (4*1024)
#define BLOCK_SIZE (4*1024)
#define INP_GPIO(g) *(gpio+((g)/10))&= ~(7<<(((g)%10)*3))
#define OUT_GPIO(g) *(gpio+((g)/10))|=(1<<(((g)%10)*3))

#define GPIO_SET *(gpio+7+1)   //sets bits which are 1 ignores bits which are 0
#define GPIO_CLR *(gpio+10+1)  //clears bits which are 1 ignores bits which are 0

/*
#define INIT_LED_REG 0xfe200010
#define INIT_LED_VAL 0x40
#define SET_LED_REG 0xfe200020 
#define SET_LED_VAL 0x400
#define CLR_LED_REG 0xfe20002c
#define CLR_LED_VAL 0x400
*/


int main(int argc,char **argv)
{
    unsigned int *gpio;
    int g,rep;
    int mem_fd=open("/dev/mem",O_RDWR|O_SYNC);
    gpio=mmap(
                NULL,                 //Any adddress in our space will do
                PAGE_SIZE,            //Map length
                PROT_READ|PROT_WRITE, //Enable reading & writing to mapped memory
                MAP_SHARED,           //Shared with other processes
                mem_fd,               //File to map
                GPIO_BASE             //Offset to GPIO peripheral
            );
    
    INP_GPIO(42);//must use INP_GPIO before we can use OUT_GPIO
    OUT_GPIO(42);
    
    if (argv[1] > 0 )
    {
        GPIO_SET = 1<<10;
    }
    else
    {
        GPIO_CLR = 1<<10;
    }
    
    return 0;
}

/*
int main(int argc,char **argv)
{
    unsigned int *gpio;
    int g,rep;
    int mem_fd=open("/dev/mem",O_RDWR|O_SYNC);
    gpio=mmap(
                NULL,                 //Any adddress in our space will do
                PAGE_SIZE,            //Map length
                PROT_READ|PROT_WRITE, //Enable reading & writing to mapped memory
                MAP_SHARED,           //Shared with other processes
                mem_fd,               //File to map
                GPIO_BASE             //Offset to GPIO peripheral
            );
    INP_GPIO(42);//must use INP_GPIO before we can use OUT_GPIO
    OUT_GPIO(42);
    GPIO_CLR = 1<<10;
    return 0;
}
*/