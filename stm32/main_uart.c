// STM32F4Disco: DO NOT use USART1 which is also used by USB OTG
// http://www.micromouseonline.com/2013/05/05/using-usart1-on-the-stm32f4discovery/

#include "uart.h"

void my_printS(short s)
{
  int k; char c;
  for(k=3;k>=0;k--)
  {
	  c=s>>(4*k)&0x0f;
	  if(c<10) mon_putchar(c+'0');
		else mon_putchar(c+'A'-10);
  }
   mon_putchar('\r');
   mon_putchar('\n');
 
 }
 
void my_printD(short s)
{
	int k=10000; char c;
	while(k>0)
	{
		c=s/k; mon_putchar(c+'0');
		s=s-c*k;
		k/=10;
	}
   mon_putchar('\r');
   mon_putchar('\n');
}

int main()
{int msk;
 msk=(1<<11)|(1<<12)|(1<<13);
 clock_setup();
 init_gpio();
 usart_setup();
 while(1)
  {
	  led_set(msk);
	  
	  my_printS(0x123A);
	  
	  
	  short s;
	  for(s=0;s<=255;s++)
	  {
		  //my_printS(s);
          delay(0x2fff);
	  }
	  
	  my_printD(1000);
	  for(s=0; s<=1000;s++)
	  {
		  //my_printD(s);
          delay(0x2fff);
	  }
	
   mon_putchar('r');
   mon_putchar('\r');
   mon_putchar('\n');
   delay(0xffff);
   led_clr(msk);
   delay(0xfffff);
   
   // DDS :
   short freq = 4*10^6; //MHz
   //f=(w*914)/3505 // RATS(70.6/2^28)
   short w= (freq*3505)/914;
   my_printS(w);
   
   
   
   
  }
 return 0;
}
