// STM32F4Disco: DO NOT use USART1 which is also used by USB OTG
// http://www.micromouseonline.com/2013/05/05/using-usart1-on-the-stm32f4discovery/

int addr=0x03;

#define decimales 100LL

#define RMIN -120
#define RMAX 120
#define IMIN -200
#define IMAX 85
#define PAS 2

struct Complex {
   short r;
   short i;
};

struct Complex addComplex(struct Complex c1, struct Complex c2)
{
	struct Complex c;
	c.r = c1.r * c2.r;
	c.i = c1.i + c2.i;
	return c;
}

struct Complex multComplex(struct Complex c1, struct Complex c2)
{
	struct Complex c;
	c.r = (c1.r * c2.r)-(c1.i * c2.i);
	c.i = (c1.r * c2.i)+(c1.i * c2.r);
	return c;
}

short modCarre(struct Complex c)
{
	short mod = c.r*c.r + c.i*c.i;
	return(mod);
}

int main()
{
	init();
	
	while(1)
	{
		ledON();
		attendre(0xfffff);
		ledOFF();
		attendre(0xfffff);  
		
		short r,i;
		short k =0;
		
		mon_putchar('*');
		mon_putchar(' ');
		mon_putchar('\r');
		mon_putchar('\n');
		
		struct Complex c,zn,zp; // z1 = c;
		
		for (i=IMIN; i<=IMAX;i=i+PAS) //-1.2 à 1.2
		{
			//my_printD(i+120);
			for (r=RMIN; r<=RMAX; r=r+PAS)
			{
				//my_printD(r+200);
				k=0;
				c.r = r;
				c.i = i;
				zn=c;
				do {
					k++;
					//my_printD(k);
					zp= addComplex(multComplex(zn,zn),c);
					zn=zp;
				} while(k<16 && modCarre(zn)<1000);
				
				if(k<16) 
				{
					mon_putchar('*');
				}
				else 
				{
					mon_putchar(' ');
				}
				//mon_putchar('*');
				
			}
			mon_putchar('\r');
			mon_putchar('\n');
		}
		
		/*
		struct Complex a;
		struct Complex b;
		a.r = 10;
		a.i = 1;
		b.r=5;
		b.i = 2;
		*/
	
		//struct Complex c = addComplex(a,b);
		//my_printD(c.r);
		//my_printD(c.i);
		
		//struct Complex c = multComplex(a,b);
		//my_printD(c.r);
		//my_printD(c.i);
		
		//my_printD(modCarre(a));
   
  }
 return 0;
}
