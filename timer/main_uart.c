#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/crc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>

#define GPIO_PUPD_NONE 0x0
#define GPIO_MODE_AF1 0x1
#define GPIO_OSPEED_50MHZ   0x2
#define GPIO_OTYPE_PP   0x0

bool etatLed = false;

#ifndef ARRAY_LEN
#define ARRAY_LEN(array) (sizeof((array))/sizeof((array)[0]))
#endif

#define LED1_PORT GPIOC
#define LED1_PIN GPIO1 //GPIO2
//volatile int x=0;
static void clock_setup(void)
{
	rcc_clock_setup_in_hse_8mhz_out_72mhz();
}

static void gpio_setup(void)
{
	/* Enable GPIO clock for leds. */
	rcc_periph_clock_enable(RCC_GPIOC);

	/* Enable led as output */
	gpio_set_mode(LED1_PORT, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, LED1_PIN);
	gpio_clear(LED1_PORT, LED1_PIN);
	
	gpio_set_output_options(GPIOC, GPIO_OTYPE_PP,GPIO_OSPEED_50MHZ, GPIO1);
	gpio_mode_setup(GPIOC, GPIO_MODE_AF1, GPIO_PUPD_NONE, GPIO1);
	gpio_set_af(GPIOC, GPIO_MODE_AF1 , GPIO1);
}

static void tim_setup(void)
{
	/* Enable TIM2 clock. */
	rcc_periph_clock_enable(RCC_TIM2);

	/* Reset TIM2 peripheral to defaults. */
	rcc_periph_reset_pulse(RST_TIM2);

	/* Timer global mode:
	 * - No divider
	 * - Alignment edge
	 * - Direction up
	 * (These are actually default values after reset above, so this call
	 * is strictly unnecessary, but demos the api for alternative settings)
	 */
	timer_set_mode(TIM2, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);
	
	
	
	timer_set_oc_mode(TIM2, TIM_OC1, TIM_OCM_PWM2);
	timer_enable_oc_output(TIM2, TIM_OC1);
	timer_enable_break_main_output(TIM2);
	timer_set_oc_value(TIM2, TIM_OC1, 200);

	/*
	 * Please take note that the clock source for STM32 timers
	 * might not be the raw APB1/APB2 clocks.  In various conditions they
	 * are doubled.  See the Reference Manual for full details!
	 * In our case, TIM2 on APB1 is running at double frequency, so this
	 * sets the prescaler to have the timer run at 5kHz
	 */
	timer_set_prescaler(TIM2, 1124);

	/* Disable preload. */
	//timer_disable_preload(TIM2);
	//timer_continuous_mode(TIM2);

	/* count full range, as we'll update compare value continuously */
	timer_set_period(TIM2, 63999);

	/* Set the initual output compare value for OC1. */
	/* Counter enable. */
	timer_enable_counter(TIM2);
}



int main(void)
{
	clock_setup();
	gpio_setup();
	tim_setup();

	while (1) {
		if (timer_get_flag(TIM2, TIM_SR_CC1IF)) {
			timer_clear_flag(TIM2, TIM_SR_CC1IF);
			etatLed ^= 1;
		}
		if(etatLed){gpio_set(LED1_PORT, LED1_PIN);}
		else{gpio_clear(LED1_PORT, LED1_PIN);}
	}

	return 0;
}
