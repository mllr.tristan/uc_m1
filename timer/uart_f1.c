#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/flash.h> // definitions du timer
#include <libopencm3/stm32/spi.h>
#include "uart.h"

#define usart1

int msk;

int spi_txrx(char c)
{
	spi_send(SPI2, c);
	return(spi_read(SPI2));
}

void chip_select(state)
{
	if (state) 
	{
		gpio_clear(GPIOB, GPIO12);
	}
	else
	{
		gpio_set(GPIOB,GPIO12);
	}
}

void init()
{
	msk=(1<<11)|(1<<12)|(1<<13);
	clock_setup();
	init_gpio();
	usart_setup();
	
	
	//static void clock_setup(void){
	rcc_clock_setup_in_hse_8mhz_out_72mhz(); //STM32F103
	rcc_periph_clock_enable(RCC_GPIOB);
	rcc_periph_clock_enable(RCC_AFIO);
	rcc_periph_clock_enable(RCC_SPI2);
	//}
	
	//static void spi_setup(void) // SS=PB12 SCK=PB13, MISO=PB14, MOSI=PB15{
	gpio_set_mode(GPIOB,GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO13|GPIO15);
	gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO12);
	gpio_set_mode(GPIOB, GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO14);
	spi_reset(SPI2);
	spi_init_master(SPI2, SPI_CR1_BAUDRATE_FPCLK_DIV_128, SPI_CR1_CPOL_CLK_TO_0_WHEN_IDLE, SPI_CR1_CPHA_CLK_TRANSITION_2, SPI_CR1_DFF_8BIT, SPI_CR1_MSBFIRST);
	spi_enable(SPI2);
	//}
}

void attendre (long hex)
{
	delay(hex);
}


void ledON()
{
	led_set(msk);
}

void ledOFF()
{
	led_clr(msk);
}

void core_clock_setup(void)
{
#ifdef STM32F10X_LD_VL
 rcc_clock_setup_in_hse_8mhz_out_24mhz();  // STM32F100 discovery
#else
 rcc_clock_setup_in_hse_8mhz_out_72mhz();  // STM32F103
#endif
}

void mon_putchar(unsigned char ch)
{
	usart_send_blocking(USART1, ch);
//	usart_send_blocking(USART2, ch);
}

void mon_puts(char *buf)
{
	int i=0; // nbre de caractère initialisé à 0
	char tmp=0; // caractere actuels
	do
	{
		tmp=buf[i];
		i++;
		mon_putchar(tmp);
	}
	while(tmp !='\0'); // on boucle tant qu'on n'est pas arrivé à l'\0
}

void delay(unsigned int delay)
{
	volatile unsigned int i;
	for(i=0;i<delay;i++)
		__asm__("nop");
}


void init_gpio(void)
{
	/* Setup GPIO for LED. */
 gpio_set_mode(GPIOC,GPIO_MODE_OUTPUT_2_MHZ,GPIO_CNF_OUTPUT_PUSHPULL,GPIO1|GPIO2);

#ifdef usart1
  gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
      GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_USART1_TX);
#else
  gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
      GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_USART2_TX);
#endif
}

void clock_setup(void)
{
// for STM32F4-Disco
// 	rcc_clock_setup_hse_3v3(&rcc_hse_8mhz_3v3[RCC_CLOCK_3V3_168MHZ]);

// for DDS-based project
	core_clock_setup();
	/* Enable GPIOD clock for LED & USARTs. */
#ifdef netwk
	rcc_periph_clock_enable(RCC_GPIOA);
#else
	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_GPIOC);
#endif

	/* Enable clocks for USART2. */
	rcc_periph_clock_enable(RCC_USART1);
	rcc_periph_clock_enable(RCC_USART2);
}


void led_set(int msk)
{
#ifdef netwk
	gpio_set(GPIOA, msk);
#else
	gpio_set(GPIOC, GPIO1|GPIO2);
#endif
}

void led_clr(int msk)
{
#ifdef netwk
	gpio_clear(GPIOA, msk);
#else
	gpio_clear(GPIOC, GPIO1|GPIO2);
#endif
}

void usart_setup(void)
{
	/* Setup USART1 parameters. */
	usart_set_baudrate(USART1, 115200);
	usart_set_databits(USART1, 8);
	usart_set_stopbits(USART1, USART_STOPBITS_1);
	usart_set_mode(USART1, USART_MODE_TX_RX);
	usart_set_parity(USART1, USART_PARITY_NONE);
	usart_set_flow_control(USART1, USART_FLOWCONTROL_NONE);

	/* Finally enable the USART. */
	usart_enable(USART1);
	
        /* Setup USART2 parameters. */
	usart_set_baudrate(USART2, 115200);
	usart_set_databits(USART2, 8);
	usart_set_stopbits(USART2, USART_STOPBITS_1);
	usart_set_mode(USART2, USART_MODE_TX_RX);
	usart_set_parity(USART2, USART_PARITY_NONE);
	usart_set_flow_control(USART2, USART_FLOWCONTROL_NONE);

	/* Finally enable the USART. */
	usart_enable(USART2);
}
